#!/bin/sh
export PATH=$PATH:$PWD/bin

# Clang-format command
if command -v clang-format &> /dev/null; then
  clang_command="clang-format" 
else
  clang_command="/opt/rh/llvm-toolset-7.0/root/usr/bin/clang-format"
fi

alias formatAll="find ${PH2ACF_BASE_DIR} -iname *.h -o -iname *.cc | xargs ${clang_command} -i"
